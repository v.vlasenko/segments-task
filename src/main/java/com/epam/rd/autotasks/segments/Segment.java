package com.epam.rd.autotasks.segments;

class Segment {
    private final double x;
    private final double y;
    private final double x1;
    private final double y1;

    public Segment(Point start, Point end) {

        if (start == end) {
            throw new IllegalArgumentException();
        }
        this.x = start.getX();
        this.y = start.getY();
        this.x1 = end.getX();
        this.y1 = end.getY();

    }

    double length() {
        return Math.sqrt((x1 - x) * (x1 - x) + (y1 - y) * (y1 - y));
    }

    Point middle() {

        var middleX = (x + x1) / 2;
        var middleY = (y + y1) / 2;
        return new Point(middleX, middleY);
    }

    Point intersection(Segment another) {

        var thisSlope = (y1 - y) / (x1 - x);
        var anotherSlope = (another.y1 - another.y) / (another.x1 - another.x);

        var thisY = thisSlope * this.x + this.y;
        var anotherY = anotherSlope * another.x + another.y;
        if (thisY != anotherY) {
            return null;
        }

        var intersectX = (another.x - this.x) / (thisSlope - anotherSlope);
        var intersectY = this.x * intersectX + this.y;
        return new Point(intersectX, intersectY);

    }
}